package ru.kpfu.itis.khayrullincrawler.entity;

import java.util.Set;

public class MatrixElement {
    private String value;
    private Set<String> references;
    private double pageRank;

    public MatrixElement(String value, Set<String> references) {
        this.value = value;
        this.references = references;
    }

    public MatrixElement(String value, Set<String> references, double pageRank) {
        this.value = value;
        this.references = references;
        this.pageRank = pageRank;
    }

    public String getValue() {
        return value;
    }

    public void setReferences(Set<String> references) {
        this.references = references;
    }

    public Set<String> getReferences() {
        return references;
    }

    public double getPageRank() {
        return pageRank;
    }

    public void setPageRank(double pageRank) {
        this.pageRank = pageRank;
    }
}
