package ru.kpfu.itis.khayrullincrawler.service;

import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public interface JsoupService {

    Document getDocument(String url);

    String crawl(String url, Integer pageCount);

    void crawl();

    Set<String> getUrls(Document document);

    String parseDoc(Document document);
}
