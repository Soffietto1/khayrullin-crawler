package ru.kpfu.itis.khayrullincrawler.service;

import ru.kpfu.itis.khayrullincrawler.entity.MatrixElement;

import java.util.List;

public interface PageRankService {
    void calculatePageRank(List<MatrixElement> matrix);
}
