package ru.kpfu.itis.khayrullincrawler.service.impl;

import ru.kpfu.itis.khayrullincrawler.service.SimilarityService;
import ru.kpfu.itis.khayrullincrawler.service.TfIdfService;
import ru.kpfu.itis.khayrullincrawler.util.FileUtil;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SimilarityServiceImpl implements SimilarityService {

    private double cosineSimilarity(Double[] vectorA, Double[] vectorB) {
        double dotProduct = 0.0;
        double normA = 0.0;
        double normB = 0.0;
        for (int i = 0; i < vectorA.length; i++) {
            dotProduct += vectorA[i] * vectorB[i];
            normA += Math.pow(vectorA[i], 2);
            normB += Math.pow(vectorB[i], 2);
        }
        return dotProduct / (Math.sqrt(normA) * Math.sqrt(normB));
    }


    //docs must be proccessed with language proccessor
    @Override
    public void printGreatest(int amount, Map<String, List<Double>> docs, String query) {
        List<Double> queryVector = docs.get(TfIdfService.QUERY_TAG);

        LinkedHashMap map = docs.entrySet().stream()
                .filter(stringListEntry -> !stringListEntry.getKey().equals(TfIdfService.QUERY_TAG))
                .sorted((o1, o2) -> {
                    double o1Similarity = cosineSimilarity(toArray(queryVector), toArray(o1.getValue()));
                    double o2Similarity = cosineSimilarity(toArray(queryVector), toArray(o2.getValue()));

                    if (o1Similarity == o2Similarity) {
                        return 0;
                    } else {
                        if (o1Similarity > o2Similarity) {
                            return -1;
                        } else {
                            return 1;
                        }
                    }
                }).limit(amount)
                .collect(Collectors.toMap(
                        Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

        FileUtil.writeQueryWithSimilarity(map, TfIdfService.QUERY_TAG, queryVector);

    }

    private Double[] toArray(List<Double> list) {
        return list.stream().toArray(Double[]::new);
    }
}
