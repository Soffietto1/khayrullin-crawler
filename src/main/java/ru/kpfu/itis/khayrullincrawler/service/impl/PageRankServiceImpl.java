package ru.kpfu.itis.khayrullincrawler.service.impl;

import ru.kpfu.itis.khayrullincrawler.entity.MatrixElement;
import ru.kpfu.itis.khayrullincrawler.service.PageRankService;
import ru.kpfu.itis.khayrullincrawler.util.FileUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PageRankServiceImpl implements PageRankService {

    private final double DAMPING_FACTOR = 0.85;

    @Override
    public void calculatePageRank(List<MatrixElement> matrix) {
        // iteration 0
        for (final MatrixElement e : matrix) {
            e.setReferences(e.getReferences().stream()
                    .filter(r -> containsInMatrix(r, matrix))
                    .collect(Collectors.toSet()));
            e.setPageRank(1.0 / matrix.size());
        }
        // iteration 1
        List<MatrixElement> resultMatrix = doOneIteration(matrix);

        // iteration 2
        resultMatrix = doOneIteration(resultMatrix);
        FileUtil.writeToPageRank(resultMatrix);
    }

    private List<MatrixElement> doOneIteration(List<MatrixElement> matrix) {
        final List<MatrixElement> thisIterationMatrix = new ArrayList<>();
        for (final MatrixElement e : matrix) {
            final List<MatrixElement> parentsList = new ArrayList<>();
            for (final MatrixElement potentialParent : matrix) {
                if (potentialParent != e && potentialParent.getReferences().contains(e.getValue())) {
                    parentsList.add(potentialParent);
                }
            }
            double pageRank = 0;
            for (MatrixElement parent : parentsList) {
                pageRank += parent.getPageRank() / parent.getReferences().size();
            }
            pageRank = (1 - DAMPING_FACTOR) + DAMPING_FACTOR * pageRank;
            thisIterationMatrix.add(new MatrixElement(e.getValue(), e.getReferences(), pageRank));
        }
        return thisIterationMatrix;
    }

    private boolean containsInMatrix(String url, List<MatrixElement> matrix) {
        for (MatrixElement e : matrix) {
            if (e.getValue().equals(url)) {
                return true;
            }
        }
        return false;
    }
}
