package ru.kpfu.itis.khayrullincrawler.service.impl;

import ru.kpfu.itis.khayrullincrawler.service.LanguageProccessingService;
import ru.kpfu.itis.khayrullincrawler.service.TfIdfService;
import ru.kpfu.itis.khayrullincrawler.util.FileUtil;
import ru.kpfu.itis.khayrullincrawler.util.NoPagesException;

import java.util.*;

public class TfIdfServiceImpl implements TfIdfService {

    private List<List<String>> allDocs;
    private Set<String> uniqueWords;
    private LanguageProccessingService languageProccessingService;
    private static final boolean IS_TEST = false;

    public TfIdfServiceImpl() {
        languageProccessingService = new LanguageProccessingServiceImpl();
        init();
    }

    private void init() {
        int totalPages = FileUtil.pagesCount(IS_TEST);
        if (totalPages == FileUtil.NO_PAGES) {
            throw new NoPagesException();
        }
        List<List<String>> result = new ArrayList<>();
        List<String> doc;

        for (int i = 0; i < totalPages; i++) {
            doc = languageProccessingService.parseToWords(FileUtil.readPage(i, IS_TEST));
            result.add(doc);
        }
        allDocs = result;
        uniqueWords = languageProccessingService.getUniqueWords(allDocs);
    }


    private double tfIdf(List<String> text, String term) {
        return tf(text, term) * idf(term);
    }

    private double tf(List<String> text, String term) {
        double result = 0;
        for (String word : text) {
            if (term.equalsIgnoreCase(word))
                result++;
        }
        return result / text.size();
    }

    private double idf(String term) {
        double n = 0;
        for (List<String> doc : allDocs) {
            for (String word : doc) {
                if (term.equalsIgnoreCase(word)) {
                    n++;
                    break;
                }
            }
        }
        return Math.log(allDocs.size() / n);
    }

    @Override
    public Map<String, List<Double>> getTfIdfVectors() {
        return calculate(allDocs, uniqueWords,false);
    }

    @Override
    public Map<String, List<Double>> getTfIdfVectors(String query) {
        List<String> terms = languageProccessingService
                .parseToWords(languageProccessingService.stem(query));

        List<List<String>> docsWithQuery = new ArrayList(allDocs);
        docsWithQuery.add(terms);

        Set<String> uniqueWordsWithQuery = languageProccessingService.getUniqueWords(docsWithQuery);
        return calculate(docsWithQuery, uniqueWordsWithQuery, true);
    }

    private Map<String, List<Double>> calculate(List<List<String>> docs, Set<String> uniqueWords, boolean isQuery) {
        Map<String, List<Double>> result = new HashMap<>();
        List<Double> vector = new ArrayList<>();

        int docCount = 0;
        String docName;
        for (List<String> doc : docs) {
            for (String word : uniqueWords) {
                vector.add(tfIdf(doc, word));
            }
            if (isQuery && docCount == docs.size() - 1) {
                docName = QUERY_TAG;
            } else {
                docName = Integer.toString(docCount);
            }
            result.put(docName, new ArrayList<>(vector));
            docCount++;
            System.out.println(docName + " документ обработан ");
            vector.clear();
        }
        return result;
    }
}
