package ru.kpfu.itis.khayrullincrawler.service;

import java.util.List;
import java.util.Map;

public interface TfIdfService {
    String QUERY_TAG = "query";

    Map<String, List<Double>> getTfIdfVectors();

    Map<String, List<Double>> getTfIdfVectors(String query);
}
