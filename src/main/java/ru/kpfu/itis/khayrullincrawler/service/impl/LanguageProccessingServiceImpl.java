package ru.kpfu.itis.khayrullincrawler.service.impl;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.ru.RussianAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.util.AttributeSource;
import org.apache.tika.langdetect.OptimaizeLangDetector;
import org.apache.tika.language.detect.LanguageDetector;
import org.apache.tika.language.detect.LanguageResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.kpfu.itis.khayrullincrawler.service.LanguageProccessingService;
import ru.kpfu.itis.khayrullincrawler.util.Language;

import java.io.IOException;
import java.io.StringReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static ru.kpfu.itis.khayrullincrawler.util.ISO_639_1_LanguagesCodes.CODE_ENGLISH;
import static ru.kpfu.itis.khayrullincrawler.util.ISO_639_1_LanguagesCodes.CODE_RUSSIAN;

/**
 * Created by Ruslan on 17.04.2017.
 */
public class LanguageProccessingServiceImpl implements LanguageProccessingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(JsoupServiceImpl.class);
    //ненужные символы
    private static final String SYMBOLS = "[^а-яА-Яa-zA-Z1-9\\s]";
    private static final Language DEFAULT_LANGUAGE = Language.RUSSIAN;
    private static final String FIELD_NAME = "contents";
    private static final String WORDS_SEPARATOR = " ";
    private static final String PARSING_EXCEPTION_MESSAGE = "Parsing gone wrong: ";

    private Analyzer analyzer;

    @Override
    public String stem(String doc) {
        return stem(doc, DEFAULT_LANGUAGE);
    }

    @Override
    public String stem(String doc, Language language) {
        switch (language) {
            case ENGLISH:
                analyzer = new EnglishAnalyzer();
                break;
            case RUSSIAN:
                analyzer = new RussianAnalyzer();
                break;
        }

        try {
            doc = doc.replaceAll(SYMBOLS, "");
            StringBuilder result = new StringBuilder();
            TokenStream stream = analyzer.tokenStream(FIELD_NAME, new StringReader(doc));
            stream.reset();

            while (stream.incrementToken()) {
                AttributeSource token = stream.cloneAttributes();
                CharTermAttribute term = token.addAttribute(CharTermAttribute.class);
                result.append(term.toString()).append(WORDS_SEPARATOR);
            }
            stream.close();
            return result.toString();

        } catch (IOException e) {
            LOGGER.info(PARSING_EXCEPTION_MESSAGE, e);
            return doc;
        }
    }

    @Override
    public Language recogniseLanguage(String doc) {
        LanguageDetector detector = new OptimaizeLangDetector().loadModels();
        LanguageResult result = detector.detect(doc);

        String langCode = result.getLanguage();
        switch (langCode) {
            case CODE_RUSSIAN:
                return Language.RUSSIAN;
            case CODE_ENGLISH:
                return Language.ENGLISH;
            default:
                return Language.ENGLISH;
        }
    }

    @Override
    public List<String> parseToWords(String doc) {
        return Arrays.asList(doc.split(" "));
    }

    @Override
    public Set<String> getUniqueWords(List<List<String>> docs) {
        Set<String> uniqueWords = new HashSet<>();
        for (List<String> doc : docs) {
            for (String word : doc) {
                uniqueWords.add(word.toLowerCase());
            }
        }
        return uniqueWords;
    }

}
