package ru.kpfu.itis.khayrullincrawler.service.impl;

import Jama.Matrix;
import Jama.SingularValueDecomposition;
import ru.kpfu.itis.khayrullincrawler.service.MatrixService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MatrixServiceImpl implements MatrixService {
    @Override
    public Matrix createMatrix(List<List<Double>> data) {
        return new Matrix(toArray(data));
    }

    @Override
    public Matrix createMatrix(Map<String, List<Double>> data) {
        return new Matrix(toArray(data));
    }

    private double[][] toArray(List<List<Double>> data) {
        int n = data.size();
        int m = data.get(0).size();
        double[][] res = new double[n][m];

        int i = 0;
        int j = 0;
        for (List<Double> vector : data) {
            for (Double value : vector) {
                res[i][j] = value;
                j++;
            }
            j = 0;
            i++;
        }
        return res;
    }

    private double[][] toArray(Map<String, List<Double>> data) {
        List<List<Double>> list = new ArrayList<>();
        Set<String> keySet = data.keySet();
        for (String key : keySet) {
            list.add(data.get(key));
        }
        return toArray(list);
    }

    @Override
    public SingularValueDecomposition getSingularValueDecomposition(Matrix matrix) {
        return matrix.svd();
    }
}
