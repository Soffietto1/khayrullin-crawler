package ru.kpfu.itis.khayrullincrawler.service;

import org.springframework.stereotype.Service;
import ru.kpfu.itis.khayrullincrawler.util.Language;

import java.util.List;
import java.util.Set;

@Service
public interface LanguageProccessingService {

    String stem(String doc);

    String stem(String doc, Language language);

    Language recogniseLanguage(String doc);

    List<String> parseToWords(String doc);

    Set<String> getUniqueWords(List<List<String>> docs);

}
