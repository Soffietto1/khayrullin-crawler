package ru.kpfu.itis.khayrullincrawler.service.impl;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.kpfu.itis.khayrullincrawler.entity.MatrixElement;
import ru.kpfu.itis.khayrullincrawler.service.JsoupService;
import ru.kpfu.itis.khayrullincrawler.service.LanguageProccessingService;
import ru.kpfu.itis.khayrullincrawler.service.PageRankService;
import ru.kpfu.itis.khayrullincrawler.util.FileUtil;
import ru.kpfu.itis.khayrullincrawler.util.Language;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class JsoupServiceImpl implements JsoupService {

    private static final Logger LOGGER = LoggerFactory.getLogger(JsoupServiceImpl.class);
    private final List<String> URLS_TO_VISIT = new ArrayList<>();
    private final List<String> URLS_VISITED = new ArrayList<>();
    private final Integer PAGES_MAX;
    private final String START;
    private final Map<String, String> INVERTED_INDEX_MAP = new HashMap<>();
    private final List<MatrixElement> MATRIX = new ArrayList<>();
    //    @Autowired пока JsoupServiceImpl создается чрез "new" никаких аутовайредов
    private final LanguageProccessingService languageProccessingService = new LanguageProccessingServiceImpl();
    private final PageRankService pageRankService = new PageRankServiceImpl();
    private final List<String> texts = new ArrayList<>();


    public JsoupServiceImpl(String start, Integer pages) throws IOException {
        START = start;
        PAGES_MAX = pages;
    }

    @Override
    public Document getDocument(String url) {
        Document doc = null;
        try {
            Connection connection = Jsoup.connect(url);
            doc = connection.get();
        } catch (IOException ex) {
            LOGGER.info("Document is null");
        }
        return doc;
    }

    @Override
    public String crawl(String url, Integer pageCount) {
        LOGGER.info(String.format("Parsing document #%s", pageCount));
        LOGGER.info("Parsing " + url);
        final Document doc = getDocument(url);
        Set<String> urls = getUrls(doc);
        MATRIX.add(new MatrixElement(url, new HashSet<>(urls)));
        urls = urls.stream()
                .filter(u -> !URLS_TO_VISIT.contains(u) && !URLS_TO_VISIT.contains(u))
                .collect(Collectors.toSet());
        URLS_TO_VISIT.addAll(urls);
        URLS_VISITED.add(url);
        final String preparedText = prepareText(parseDoc(doc));
        FileUtil.writeToPages(preparedText, pageCount);
        addToInvertedIndex(preparedText, pageCount);
        return preparedText;
    }

    @Override
    public void crawl() {
        FileUtil.cleanPagesDir();
        texts.clear();
        LOGGER.info("Parsing " + START);
        final Document doc = getDocument(START);
        final Set<String> urls = getUrls(doc);
        URLS_TO_VISIT.addAll(urls);
        MATRIX.add(new MatrixElement(START, new HashSet<>(urls)));
        for (int i = 0; URLS_VISITED.size() < PAGES_MAX - 1 && URLS_TO_VISIT.size() > i && doc != null; i++) {
            String text = crawl(URLS_TO_VISIT.get(i), i);
            texts.add(text);
        }
        FileUtil.writeToIndex(URLS_VISITED);
        FileUtil.writeToInvertedIndex(INVERTED_INDEX_MAP);
        URLS_VISITED.clear();
        URLS_TO_VISIT.clear();
        pageRankService.calculatePageRank(MATRIX);
    }

    @Override
    public Set<String> getUrls(Document document) {
        return document == null ? new HashSet<>() : document.select("a[href]")
                .stream()
                .map(element -> element.attr("href"))
                .filter(Objects::nonNull)
                .filter(url -> url.startsWith("http"))
                .filter(url -> !url.matches(".*(\\.(css|js|gif|jpg|png|mp3|mp4|zip|gz))$"))
                .collect(Collectors.toSet());
    }

    @Override
    public String parseDoc(Document document) {
//        String body = getDocument(url).body().toString();
//        return Jsoup.clean(body, "", Whitelist.none(), new Document.OutputSettings().prettyPrint(false));
        if (document != null && document.body() != null) {
            return document.body().text();
        }
        return "";
    }

    private String prepareText(String text) {
        Language language = languageProccessingService.recogniseLanguage(text);
        return languageProccessingService.stem(text, language);
    }

    private void addToInvertedIndex(String text, int pageCount) {
        String[] words = getTextWords(text);
        for (String word : new HashSet<>(Arrays.asList(words))) {
            String value = INVERTED_INDEX_MAP.get(word);
            if (value != null) {
                INVERTED_INDEX_MAP.put(word, value + ", " + pageCount);
            } else {
                INVERTED_INDEX_MAP.put(word, Integer.toString(pageCount));
            }
        }
    }

    private String[] getTextWords(String text) {
        return text.split(" ");
    }

    public double tf(String text, String word) {
        String[] textWord = getTextWords(text);
        return textWord.length / (double) getWordCountInText(text, word);
    }

    public double idf(String word) {
        long allWordsCount = 0;
        long wordInAllTextCount = 0;
        for (String text : texts) {
            wordInAllTextCount += getWordCountInText(text, word);
            allWordsCount += getTextWords(text).length;
        }
        return wordInAllTextCount / (double) allWordsCount;
    }

    private long getWordCountInText(String text, String word) {
        String[] textWord = getTextWords(text);
        return Arrays.stream(textWord).filter(s -> s.equalsIgnoreCase(word)).count();
    }

    public double tfIdf(String text, String word) {
        return tf(text, word) / idf(word);
    }

    private List<Double> computeDocVector(String doc) {
        List<Double> res = new ArrayList<Double>();
        for (String word : getTextWords(doc)) {
            res.add(tfIdf(doc, word));
        }
        return res;
    }

}
