package ru.kpfu.itis.khayrullincrawler.service;

import Jama.Matrix;
import Jama.SingularValueDecomposition;

import java.util.List;
import java.util.Map;

public interface MatrixService {

    Matrix createMatrix(List<List<Double>> data);

    Matrix createMatrix(Map<String, List<Double>> data);

    //Зачем? Да фиг его знает, хочу и сделаю
    SingularValueDecomposition getSingularValueDecomposition(Matrix matrix);
}
