package ru.kpfu.itis.khayrullincrawler.util;

import static ru.kpfu.itis.khayrullincrawler.util.ISO_639_1_LanguagesCodes.CODE_ENGLISH;
import static ru.kpfu.itis.khayrullincrawler.util.ISO_639_1_LanguagesCodes.CODE_RUSSIAN;

public enum Language {
    RUSSIAN(CODE_RUSSIAN), ENGLISH(CODE_ENGLISH);

    private String langCode;

    Language(String langCode) {
        this.langCode = langCode;
    }
}
