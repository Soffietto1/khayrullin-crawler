package ru.kpfu.itis.khayrullincrawler.util;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.kpfu.itis.khayrullincrawler.entity.MatrixElement;

import java.io.*;
import java.util.List;
import java.util.Map;

public class FileUtil {

    private static final String PAGES_DIR = "src/main/resources/pages/";
    private static final String RESULT_TEXT = "src/main/resources/result/similarity.txt";
    private static final String INDEX_TXT = "src/main/resources/index.txt";
    private static final String INVERTED_INDEX_TXT = "src/main/resources/inverted-index.txt";
    private static final String PAGE_RANK = "src/main/resources/page-rank.txt";

    public static final int NO_PAGES = -13;

    private static final Logger LOGGER = LoggerFactory.getLogger(FileUtil.class);
    private static final String TEST_PAGES_DIR = "src/main/resources/pages_test/";

    public static void writeToIndex(List<String> urls) {
        try (Writer writer = new FileWriter(new File(INDEX_TXT))) {
            for (String url : urls) {
                writer.write(url + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeToPages(String text, Integer pageCount) {
        try (Writer writer = new FileWriter(PAGES_DIR + toTxt(pageCount))) {
            writer.write(text);
        } catch (IOException e) {
            LOGGER.error(String.format("ERROR WRITING PAGE FILE №%s", pageCount));
        }
    }

    public static void writeToInvertedIndex(Map<String, String> invertedIndexMap) {
        try (Writer writer = new FileWriter(new File(INVERTED_INDEX_TXT))) {
            for (Map.Entry entry : invertedIndexMap.entrySet()) {
                writer.write(entry.getKey() + " : " + entry.getValue() + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeToPageRank(List<MatrixElement> elements) {
        elements.sort((a, b) -> (int) Math.signum(b.getPageRank() - a.getPageRank()));
        try (Writer writer = new FileWriter(new File(PAGE_RANK))) {
            for (MatrixElement e : elements) {
                writer.write(e.getValue() + "; PageRank = " + e.getPageRank() + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void cleanPagesDir() {
        try {
            FileUtils.cleanDirectory(new File(PAGES_DIR));
        } catch (IOException e) {
            LOGGER.error("ERROR CLEARING PAGES DIR");
        }

    }

    private static String toTxt(Integer page) {
        return page + ".txt";
    }

    public static String readPage(int pageNumber, boolean isTest) {
        String dir = resolveDir(isTest);
        try (Reader reader = new FileReader(dir + toTxt(pageNumber))) {
            StringBuilder builder = new StringBuilder();
            int chr;
            while ((chr = reader.read()) != -1) {
                builder.append((char) chr);
            }
            return builder.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int pagesCount(boolean isTest) {
        String dir = resolveDir(isTest);
        File file = new File(dir);
        if (file.isDirectory() && file.listFiles() != null) {
            return file.listFiles().length;
        } else {
            return NO_PAGES;
        }
    }

    public static void writeQueryWithSimilarity(Map<String, List<Double>> map,
                                                String queryTag, List<Double> queryVector) {

        try (Writer writer = new FileWriter(new File(RESULT_TEXT))) {

            writer.write(namedVectorToString(queryTag, queryVector));
            writer.write("------------------------------------" + "\n");

            map.entrySet().stream().forEach(stringListEntry -> {
                try {
                    writer.write(namedVectorToString(stringListEntry.getKey(), stringListEntry.getValue()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String namedVectorToString(String tag, List<Double> vector) {
        StringBuilder builder = new StringBuilder();
        builder.append(tag).append(" : ").append(vectorToString(vector)).append("\n");
        return builder.toString();
    }

    private static String vectorToString(List<Double> vector) {
        StringBuilder builder = new StringBuilder();
        for (Double value : vector) {
            builder.append(value + " ");
        }
        return builder.toString();
    }

    private static String resolveDir(boolean isTest) {
        if (isTest) {
            return TEST_PAGES_DIR;
        } else {
            return PAGES_DIR;
        }
    }
}
