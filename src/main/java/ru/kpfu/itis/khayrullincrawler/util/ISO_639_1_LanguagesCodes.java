package ru.kpfu.itis.khayrullincrawler.util;

public class ISO_639_1_LanguagesCodes {

    public static final String CODE_RUSSIAN = "ru";
    public static final String CODE_ENGLISH = "en";

}
