package ru.kpfu.itis.khayrullincrawler;

import ru.kpfu.itis.khayrullincrawler.service.SimilarityService;
import ru.kpfu.itis.khayrullincrawler.service.TfIdfService;
import ru.kpfu.itis.khayrullincrawler.service.impl.SimilarityServiceImpl;
import ru.kpfu.itis.khayrullincrawler.service.impl.TfIdfServiceImpl;

import java.util.List;
import java.util.Map;

public class KhayrullinCrawlerApplication {

//    public static void main(String[] args) throws IOException {
//        JsoupService jsoupService = new JsoupServiceImpl("https://habr.com/", 10);
//        jsoupService.crawl();
//    }
//
    public static void main(String[] args) {
        String query = "Анализ мочи";
        TfIdfService tfIdfService = new TfIdfServiceImpl();
        Map<String, List<Double>> tfIdfVectors =  tfIdfService.getTfIdfVectors(query);
        SimilarityService similarityService = new SimilarityServiceImpl();
        similarityService.printGreatest(3, tfIdfVectors,query);
    }

}

